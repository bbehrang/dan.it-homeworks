"use strict";
function name_isInvalid(name){
    return (name === null || /\d/.test(name) || name === "")
}
function age_isInvalid(age){
    return ((isNaN(age) || age < 1 || age > 120))
}
let name = prompt("Please enter your name");
while(name_isInvalid(name)) name = prompt("Please enter a valid name, you wrote: " + name);
let age = prompt("Please enter your age");
while(age_isInvalid(age)) age = prompt("Please enter a valid number, you wrote: " + age);
if(age < 18)    alert("You are not allowed to visit this website.");
else if( age >= 18 && age <=22) {
    const isContinue = confirm("Are you sure you want to continue?");
    isContinue ? alert("Welcome, " + name) : alert("You are not allowed to visit this website");
}
else alert("Welcome, "+ name);
