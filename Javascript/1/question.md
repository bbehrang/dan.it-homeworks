1-Var variables are function scoped(this includes switch, if, for and other loops). "var" variables are hoisted, meaning their declaration is always moved to the top of the scope they are located in<br>
Const variables cannot be reassigned after they are declared<br>
Let variables are block scoped, they can be reassigned and they are not hoisted<br>
2- Because var variables can be reassigned using the keyword for example:<br>
var x = 5;<br>
var x = 6;