"use strict";
function createNewUser() {
    let firstName = prompt("Please enter your first name");
    while(parseInt(firstName) || !firstName.trim() || !firstName) firstName = prompt("Please enter a valid first name, you wrote: " + firstName);

    let lastName = prompt("Please enter your last name");
    while(parseInt(lastName) || !lastName.trim() || !lastName) lastName = prompt("Please enter a valid last name, you wrote: " + lastName);

    let bDate = prompt("Please enter your birthday in the following format; dd.mm.yyyy");
    let bDateArr = bDate.split('.');

    const newUser = {
        _firstName : firstName,
        _lastName : lastName,
        birthday : new Date(bDateArr[2],bDateArr[1],bDateArr[0]),
        get firstName(){ return this._firstName},
        get lastName (){ return this._lastName},
        set setFirstName(value){ this._firstName = value },
        set setLastName(value) { this._lastName = value },
        getLogin : function() { return this.firstName.charAt(0) + this.lastName},
        getAge : function() {return new Date().getFullYear() - this.birthday.getFullYear()},
        getPassowrd : function() {
            return this.firstName + " " + this.lastName + " " + this.birthday.toLocaleDateString('ru') + " \u2192 " +
                this.firstName.charAt(0).toUpperCase() + this.lastName + this.birthday.getFullYear()
        }
    };
    return newUser;
}

const newUser = createNewUser();
console.log(newUser.getAge());
console.log(newUser.getPassowrd());

