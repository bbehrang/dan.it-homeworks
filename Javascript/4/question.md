1- Method objects allow us to modify properties of our objects or do an operation and return a result bases on these properties. Default methods of the Object type such as Keys(), values(), entries() etc allow us to do various things with our objects including iterating through keys and values, or copy values from one object to another.<br>
Some examples of the Object constructor methods:<br>
Object.create()<br> 
Object.keys()<br>
Object.values()<br>
Object.entries()<br>
Object.assign()<br>
Object.freeze()<br>
Object.seal()<br>
Object.getPrototypeOf()<br> 