function calculate(m,n,op){
    switch (op) {
        case "+" : return m + n;
        case "-" : return m - n;
        case "*" : return m * n;
        case "/" :
            if(n === 0) return "Cant divide by zero";
            else return m / n;
        default:
            return "Operation doesnt exist";
    }
}
let m = 0;
let n = 0;
do{
   m = prompt("Enter the first number(m):");
   n = prompt("Enter the second number(n)");
} while(isNaN(n) || isNaN(m));

const mFloat = parseFloat(m);
const nFloat = parseFloat(n);

let operator = prompt("Please enter operator(+,-,*,/)");
console.log(calculate(mFloat, nFloat, operator));

